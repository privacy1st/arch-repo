#!/bin/bash

source /etc/de-p1st-repo/arch-repo.cfg || exit $?


cd "${LOCAL_PKG_DIR}" || exit $?

match="0"
for PKG in ./*.pkg.tar.{xz,zst}; do
    [ -f "$PKG" ] || { echo "No match for pattern $PKG"; continue; }  # alternatively: shopt -s nullglob

    # if we are here, there is at least one match!
    match="1"
    break
done

if [ "$match" = "1" ]; then
  shopt -s nullglob
  # print all matching
  echo ./*.pkg.tar.{xz,zst}
fi
