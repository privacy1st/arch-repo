function first_word() {
  # return: The first word of $1.
  #   In detail: "" if $1 starts with a space. Otherwise: All characters until the first space of $1.
  #
  # source: https://unix.stackexchange.com/a/201744/315162

  echo "${1%% *}"
}


# Inspired by: https://stackoverflow.com/questions/2172352/in-bash-how-can-i-check-if-a-string-begins-with-some-value/18558871#18558871
#
# $1 begins with $2
#
beginswith() { case $1 in "$2"*) true;; *) false;; esac; }
#
# $1 ends with $2
#
endswith() { case $1 in *"$2") true;; *) false;; esac; }
