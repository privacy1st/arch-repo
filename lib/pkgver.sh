#
# get package-file with latest version for given package name
#
function latest_pkgver_path(){
  # precond: In current working directory there is a subdir "db"
  # $1: package name
  # return: stdout: path to package file
  local PKGNAME
  PKGNAME="$1"

  # get latest version for $PKGNAME
  local LATEST_PKGVER
  LATEST_PKGVER=$(latest_pkgver "${PKGNAME}") || return $?
  # get the path to package file
  local PKG
  PKG=$(cat "db/${PKGNAME}/${LATEST_PKGVER}") || return $?

  echo "${PKG}"
}

#
# get latest version of package
#
function latest_pkgver(){
  # precond: In current working directory there is a subdir "db"
  # $1: package name
  # return: stdout: latest pkgver
  local PKGNAME
  PKGNAME="$1"

  # pick one random version as starting point for the latest version
  local LATEST_PKGVER
  for PKGVER in db/"${PKGNAME}"/*; do
    PKGVER=$(basename "${PKGVER}") || return $?  # strip directory and suffix from filename
    LATEST_PKGVER="${PKGVER}"
    break
  done

  local cmp
  for PKGVER in db/"${PKGNAME}"/*; do
    PKGVER=$(basename "${PKGVER}") || return $?  # strip directory and suffix from filename

    # compare the currently known latest version
    # with the next version
    cmp=$(vercmp "${LATEST_PKGVER}" "${PKGVER}") || return $?
    # if the new version is larger, save it as LATEST_PKGVER
    if [ "${cmp}" -lt "0" ]; then
      LATEST_PKGVER="${PKGVER}"
    fi
  done

  echo "${LATEST_PKGVER}"
}
