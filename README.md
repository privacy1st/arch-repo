# Arch Linux repository manager

Special thanks to nachoparker for his article:

* [Replicate your system with self-hosted Arch Linux metapackages](https://ownyourbits.com/2019/07/21/replicate-your-system-with-self-hosted-arch-linux-metapackages/)

## Setup

Install `de-p1st-repo` on your local machine as well as on
a remote server.

Adjust [/etc/de-p1st-repo/arch-repo.conf](arch-repo.cfg) according to your needs.

Run a webserver on the server to serve static content:

* [https://hub.docker.com/_/nginx/]() -> Hosting some simple static content
* `sudo docker run --name arch-repo -v /mnt/data/live/arch-repo:/usr/share/nginx/html:ro -d nginx`

Add the newly created mirror to your `/etc/pacman.conf`:

```
[de-p1st]
SigLevel = Optional TrustAll
Server = https://arch.p1st.de
```

## Normal usage

### Check for AUR updates, build and push

Check remote repository for AUR packages that can be updated

```shell
arch-repo-vercmp
```

Then build those packages locally and push changes to remote repository

```shell
# Build on local machine in clean chroot (or with an AUR helper), e.g.
makepkg -fCcsr

# Push new packages to remote repository
arch-repo-push-new
```

## Example output

```
user@localMachine ~ % arch-repo-push-new 
de-p1st-pacman-0.0.7-2-any.pkg.tar.zst
          2.84K 100%    0.00kB/s    0:00:00 (xfr#1, to-chk=325/384)
de-p1st-repo-0.1.1-2-any.pkg.tar.zst
          5.13K 100%    4.90MB/s    0:00:00 (xfr#2, to-chk=308/384)
de-p1st-repo-0.1.1-3-any.pkg.tar.zst
          5.13K 100%    4.90MB/s    0:00:00 (xfr#3, to-chk=307/384)
new-pkg.txt
            113 100%    0.00kB/s    0:00:00 (xfr#1, to-chk=0/1)
Adding new packages to db ...
Sorting new packages by package name and package version ...
Creating file ./db/de-p1st-pacman/0.0.7-2 with content de-p1st-pacman-0.0.7-2-any.pkg.tar.zst ...
Creating file ./db/de-p1st-repo/0.1.1-2 with content de-p1st-repo-0.1.1-2-any.pkg.tar.zst ...
Creating file ./db/de-p1st-repo/0.1.1-3 with content de-p1st-repo-0.1.1-3-any.pkg.tar.zst ...
For each new package name: Add latest version to database ...
==> Extracting de-p1st.db.tar.gz to a temporary location...
==> Extracting de-p1st.files.tar.gz to a temporary location...
==> Adding package 'de-p1st-pacman-0.0.7-2-any.pkg.tar.zst'
  -> Computing checksums...
  -> Removing existing entry 'de-p1st-pacman-0.0.7-1'...
  -> Creating 'desc' db entry...
  -> Creating 'files' db entry...
==> Creating updated database file 'de-p1st.db.tar.gz'
==> Extracting de-p1st.db.tar.gz to a temporary location...
==> Extracting de-p1st.files.tar.gz to a temporary location...
==> Adding package 'de-p1st-repo-0.1.1-3-any.pkg.tar.zst'
  -> Computing checksums...
  -> Removing existing entry 'de-p1st-repo-0.1.1-1'...
  -> Creating 'desc' db entry...
  -> Creating 'files' db entry...
==> Creating updated database file 'de-p1st.db.tar.gz'
Generating index.html with links to all packages ...
```

Where the generated `new-pkg.txt` has this content:

```
de-p1st-pacman-0.0.7-2-any.pkg.tar.zst
de-p1st-repo-0.1.1-2-any.pkg.tar.zst
de-p1st-repo-0.1.1-3-any.pkg.tar.zst
```

## Removing packages

On your remote server:

```shell
PKG=xml2
REPO_NAME=de-p1st

repo-remove "${REPO_NAME}".db.tar.gz "${PKG}"
rm "${PKG}"-PKG-VERSION.pkg.tar.{xz,zst}
rm -r db/"${PKG}"/
```
